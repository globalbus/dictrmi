package org.dict.dictrmi.server;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.dict.dictrmi.contracts.IDict;
import org.dict.dictrmi.contracts.IRecord;
import org.dict.dictrmi.contracts.Record;

public class DictAdapter implements IDict {

	private Dict dict;

	public DictAdapter() {
		long start = System.currentTimeMillis();
		this.dict = new Dict();
		dict.read();
		Logger.getLogger(App.class.getName()).log(Level.INFO, String.format("StartupTime %d ms", System.currentTimeMillis()-start));
	}

	@Override
	public IRecord search(String password) throws RemoteException {
		Record temp = dict.search(password);
		if (temp != null && temp.getSubs() == null)
			dict.getSubs(temp);
		return temp;
	}

	@Override
	public String getName() throws RemoteException {
		return dict.getName();
	}

}
