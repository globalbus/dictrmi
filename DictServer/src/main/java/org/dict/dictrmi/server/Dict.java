package org.dict.dictrmi.server;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import java.nio.charset.Charset;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Locale;

import org.dict.dictrmi.contracts.IDict;
import org.dict.dictrmi.contracts.Record;

enum Mode {
	indexed, memory
}

public class Dict implements IDict {
	private static final String SOURCE_FILE = "odm.txt";
	private static final int BUFFER_LENGTH = 8196;
	private Record[] records;
	private Mode mode = Mode.indexed;

	@Override
	public String getName() {
		return "SJP Dictionary";
	}

	public Dict() {

	}

	@Override
	public Record search(String password) {
		if (password == null)
			return null;
		int len = records.length;
		int startidx = 0;
		int endidx = len - 1;

		if (password.compareTo(records[startidx].getMain()) == 0)
			return records[startidx];

		if (password.compareTo(records[endidx].getMain()) == 0)
			return records[endidx];

		Collator collator = Collator.getInstance(new Locale("pl", "PL"));
		int middle;
		String word;
		while ((endidx - startidx) != 1) {
			middle = (startidx + endidx) >> 1;
			word = records[middle].getMain();

			if (collator.compare(word, password) < 0) {
				startidx = middle;
			} else if (collator.compare(word, password) > 0) {
				endidx = middle;
			} else {
				return records[middle];
			}
		}
		return null;

	}

	public void read() {
		try (InputStream stream = ClassLoader.getSystemClassLoader()
				.getResourceAsStream(SOURCE_FILE)) {

			ArrayList<Record> records = new ArrayList<>();
			records.ensureCapacity(200000);
			ArrayList<String> subs = new ArrayList<>();
			subs.ensureCapacity(200);

			try (InputStreamReader input = new InputStreamReader(stream,
					Charset.forName("windows-1250"))) {
				char[] buffer = new char[BUFFER_LENGTH];
				int globalPointer = 0;
				int pointer = 0;
				int i = 0;
				int readLater = 0;
				int to = 0;
				boolean first = false;
				Record record = new Record();
				int read = 0;
				while (true) {
					globalPointer += read;
					read = input.read(buffer, readLater, BUFFER_LENGTH
							- readLater);
					if (read < 0) {
						break;
					}
					if (read + i < BUFFER_LENGTH)
						to = read + i;
					else
						to = BUFFER_LENGTH;
					while (i < to) {
						if (buffer[i] == ',') {
							if (mode == Mode.indexed) {
								if (!first) {
									record.setIndex(globalPointer + pointer);
									subs.add(new String(buffer, pointer, i
											- pointer));
									first = true;
								}

							} else
								subs.add(new String(buffer, pointer, i
										- pointer));
							pointer = i + 2;
							i++;
						} else if (buffer[i] == '\r') {
							if (mode == Mode.indexed) {
								if (!first) {
									record.setIndex(globalPointer + pointer);
									subs.add(new String(buffer, pointer, i
											- pointer));
								}
							} else {
								subs.add(new String(buffer, pointer, i
										- pointer));
								if (subs.size() > 1)
									record.setSubs(subs.subList(1, subs.size())
											.toArray(new String[0]));
								else
									record.setSubs(new String[0]);
							}
							record.setMain(subs.get(0));
							records.add(record);
							subs.clear();
							record = new Record();
							first = false;
							pointer = i + 2;
							i++;
						}
						i++;
					}

					if (pointer < BUFFER_LENGTH) {
						System.arraycopy(buffer, pointer, buffer, 0, BUFFER_LENGTH-pointer);
						i = i - pointer;
						pointer = 0;
						readLater = i;
					} else {
						pointer -= BUFFER_LENGTH;
						i -= BUFFER_LENGTH;
						readLater = 0;
					}

				}

			}
			this.records = records.toArray(new Record[0]);
			records.clear();
			records = null;
			subs.clear();
			subs = null;
			System.gc();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public void save() {
		try (OutputStream outstream = new FileOutputStream("odm2.txt")) {

			try (OutputStreamWriter output = new OutputStreamWriter(outstream,
					Charset.forName("windows-1250"))) {
				for (Record r : this.records) {
					output.write(r.toString());
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public void getSubs(Record record) {
		try (InputStream stream = ClassLoader.getSystemClassLoader()
				.getResourceAsStream(SOURCE_FILE)) {
			ArrayList<String> subs = new ArrayList<>();
			subs.ensureCapacity(200);
			try (InputStreamReader input = new InputStreamReader(stream,
					Charset.forName("windows-1250"))) {
				input.skip(record.getIndex());
				char[] buffer = new char[BUFFER_LENGTH];
				int pointer = 0;
				int i = 0;
				int readLater = 0;
				int to = 0;
				int read = 0;
				while (true) {
					read = input.read(buffer, readLater, BUFFER_LENGTH
							- readLater);
					if (read < 0) {
						break;
					}
					if (read + i < BUFFER_LENGTH)
						to = read + i;
					else
						to = BUFFER_LENGTH;
					while (i < to) {
						if (buffer[i] == ',') {
							subs.add(new String(buffer, pointer, i - pointer));
							pointer = i + 2;
							i++;
						} else if (buffer[i] == '\r') {
							subs.add(new String(buffer, pointer, i - pointer));
							if (subs.size() > 1)
								record.setSubs(subs.subList(1, subs.size())
										.toArray(new String[0]));
							else
								record.setSubs(new String[0]);
							subs.clear();
							return;
						}
						i++;
					}

					if (pointer < BUFFER_LENGTH) {
						System.arraycopy(buffer, pointer, buffer, 0, BUFFER_LENGTH-pointer);
						i = i - pointer;
						pointer = 0;
						readLater = i;
					} else {
						pointer -= BUFFER_LENGTH;
						i -= BUFFER_LENGTH;
						readLater = 0;
					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
