package org.dict.dictrmi.client;

import org.dict.dictrmi.client.view.QueryPanel;
import org.dict.dictrmi.client.view.ResultPanel;


public class MainFrame extends AbstractMainFrame {
	
	private QueryPanel display;
	private ResultPanel control;
	public MainFrame() {
    }
    public void setQueryPanel(QueryPanel display) {
		this.display = display;
    	if (displayPanel != null)
    		getContentPane().remove(displayPanel);
    	displayPanel = display.getPanel();
    	getContentPane().add(displayPanel, java.awt.BorderLayout.NORTH);
    	pack();
    }
    public void setResultPanel(ResultPanel controlPanel) {
		this.control = controlPanel;
    	if (this.controlPanel != null)
    		getContentPane().remove(this.controlPanel);
    	this.controlPanel = controlPanel.getPanel();
    	getContentPane().add(this.controlPanel, java.awt.BorderLayout.CENTER);
    	pack();
    }
	
}