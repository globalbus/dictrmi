package org.dict.dictrmi.client.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.dict.dictrmi.client.DictContainer;

public class QueryPanel {
	private JPanel contentPanel;
	DictContainer container;
	public QueryPanel(){
		init();
	}
	private void init(){
		contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		final JTextField text = new JTextField();
		contentPanel.add(text, BorderLayout.CENTER);
		final JButton searchButton = new JButton("Search");
		searchButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(container!=null)
					container.search(text.getText());
				
			}
			
		});
		contentPanel.add(searchButton, BorderLayout.EAST);
		text.addKeyListener( new KeyListener(){

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				if(arg0.getKeyCode()==KeyEvent.VK_ENTER)
					searchButton.doClick();
			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}});
	}
	public JPanel getPanel() {
		return contentPanel;
	}
	public void setDict(DictContainer container) {
		this.container=container;
	}
}
