package org.dict.dictrmi.client.view;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import org.dict.dictrmi.contracts.IRecord;

public class ResultPanel {
	JPanel contentPanel;
	JTextArea area;

	public ResultPanel() {
		contentPanel = new JPanel();
		contentPanel.setLayout(new BorderLayout());
		area = new JTextArea();
		area.setEditable(false);
		area.setWrapStyleWord(true);
		area.setLineWrap(true);
		contentPanel.add(area, BorderLayout.CENTER);

	}

	public JPanel getPanel() {
		return contentPanel;
	}

	public void setResult(final IRecord search) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				if (search == null)
					area.setText("Nothing Found");
				else {
					String text = search.toString();
					area.setText(text);
				}
				contentPanel.validate();
			}

		});

	}

}
