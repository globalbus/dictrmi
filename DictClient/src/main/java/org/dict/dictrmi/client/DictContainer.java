package org.dict.dictrmi.client;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.dict.dictrmi.client.view.ResultPanel;
import org.dict.dictrmi.contracts.IDict;
import org.dict.dictrmi.contracts.IRecord;

/**
 * @author globalbus Proxy Class to keep RMI object
 */
public class DictContainer {

	private ExecutorService executor;

	public DictContainer() {
//		InputStream input = ClassLoader
//				.getSystemResourceAsStream("security.policy");
//
//		try (FileOutputStream write = new FileOutputStream("security.policy")) {
//			byte[] read = new byte[input.available()];
//			input.read(read);
//			write.write(read);
//			input.close();
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		}
//
//		System.setProperty("java.security.policy", "security.policy");
//		if (System.getSecurityManager() == null) {
//			SecurityManager security = new RMISecurityManager();
//			System.setSecurityManager(security);
//		}
		executor = Executors.newSingleThreadExecutor();
	}

	IDict dict;
	String server;
	ResultPanel panel;

	public void search(final String password) {
		executor.execute(new Runnable() {
			int failSafe = 1;

			@Override
			public void run() {
				try {
					IRecord result = dict.search(password);
					panel.setResult(result);
					failSafe = 2;
				} catch (RemoteException e) {
					getDict(server);
					if (failSafe-- > 0)
						run();
				}
			}

		});

	}

	public String getName() {
		try {
			return dict.getName();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void getDict(String server) {
		this.server = server;
		try {
			dict = (IDict) Naming
					.lookup(String.format("rmi://%s/Dict", server));
		} catch (Exception e) {
			System.out.println("Client exception: " + e);
			System.exit(0);
		}
	}

	public void setResultPanel(ResultPanel panel) {
		this.panel = panel;
	}
}
