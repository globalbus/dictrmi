package org.dict.dictrmi.client;

import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.dict.dictrmi.client.view.QueryPanel;
import org.dict.dictrmi.client.view.ResultPanel;

public class App {

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		Logger logger = Logger.getLogger(App.class.getName());
		String dialog = JOptionPane.showInputDialog("Wpisz nazwę serwera/adres","localhost");
		final DictContainer container = new DictContainer();
		container.getDict(dialog);
		/* Set the Nimbus look and feel */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager
					.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (javax.swing.UnsupportedLookAndFeelException
				| IllegalAccessException | ClassNotFoundException
				| InstantiationException ex) {
			logger.log(java.util.logging.Level.SEVERE, null, ex);
		}

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				MainFrame mainFrame = new MainFrame();
				mainFrame.setTitle(container.getName());
				QueryPanel query = new QueryPanel();
				query.setDict(container);
				ResultPanel result = new ResultPanel();
				container.setResultPanel(result);
				mainFrame.setQueryPanel(query);
				mainFrame.setResultPanel(result);
				mainFrame.setBounds(0, 0, 400, 400);
				mainFrame.setVisible(true);
			}
		});
	}
}