package org.dict.dictrmi.contracts;


public class Record implements IRecord {
	private static final long serialVersionUID = 1L;
	private String main;
	private int index;
	@Override
	public String getMain() {
		return main;
	}
	
	public void setMain(String main) {
		this.main = main;
	}
	@Override
	public String[] getSubs() {
		return subs;
	}

	public void setSubs(String[] subs) {
		this.subs = subs;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	private String[] subs;
	@Override
	public String toString(){
		String text = main;
		for (int i=0; i<subs.length; i++) {
			text +=  ", "+ subs[i];
		}
		text+="\r\n";
		return text;
	}
}