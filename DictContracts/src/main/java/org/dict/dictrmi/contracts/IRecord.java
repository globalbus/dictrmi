package org.dict.dictrmi.contracts;

import java.io.Serializable;

public interface IRecord extends Serializable{

	String getMain();

	String[] getSubs();

}
