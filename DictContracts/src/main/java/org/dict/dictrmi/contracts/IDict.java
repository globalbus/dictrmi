package org.dict.dictrmi.contracts;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IDict extends Remote{

	IRecord search(String password) throws RemoteException;

	String getName() throws RemoteException;

}
